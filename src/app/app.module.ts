import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { FolderViewComponent } from './folder-view/folder-view.component';
import { FolderItemComponent } from './folder-view/folder-item/folder-item.component';
import { FolderNavigationComponent } from './folder-view/folder-navigation/folder-navigation.component';
import { HeaderComponent } from './header/header.component';
import {FormsModule} from "@angular/forms";

@NgModule({
  declarations: [
    AppComponent,
    FolderViewComponent,
    FolderItemComponent,
    FolderNavigationComponent,
    HeaderComponent
  ],
  imports: [
    BrowserModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
