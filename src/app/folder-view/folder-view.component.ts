import { Component, OnInit } from '@angular/core';
@Component({
  selector: 'app-folder-view',
  templateUrl: './folder-view.component.html',
  styleUrls: ['./folder-view.component.css']
})
export class FolderViewComponent implements OnInit {
  public folders: {name: string, backtrack: string[], children: object[]};
  public currentFolder: {name: string, backtrack: object, children: object};

  currentPath;
  pathStr = '[root]';

  currentFolderChanged(newData: {name: string, backtrack: object, children: object}) {
    console.log('currentFolderChanged');
    console.log(newData);
    this.currentPath = newData.backtrack;
    localStorage.setItem('currentPath', JSON.stringify(newData.backtrack));
    this.pathStr = this.currentPath.join(' / ');
    console.log(this.pathStr);
    this.currentFolder = newData;
  }

  constructor() {
    this.folders = JSON.parse(localStorage.getItem('folder'));
    console.log(this.folders);
    if (this.folders !== null) {
      this.currentFolder = this.folders;
    } else {
      this.currentFolder = {name: '.', backtrack: [], children: []};
    }

    localStorage.setItem('currentPath', JSON.stringify(['[root]']));
  }

  ngOnInit() {

  }

}
