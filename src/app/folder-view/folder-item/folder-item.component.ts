import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {forEach} from '@angular/router/src/utils/collection';

@Component({
  selector: 'app-folder-item',
  templateUrl: './folder-item.component.html',
  styleUrls: ['./folder-item.component.css']
})
export class FolderItemComponent implements OnInit {
  errorMsg: boolean;
  hasChildren: boolean;
  public newFolderName = '';
  showPopup = false;
  newFolder: object;
  updatedBt: string[];
  @Input() curFolder: {name: string, backtrack: object, children: object};
  @Output() onCurrentFolderChange = new EventEmitter<{name: string, backtrack: object, children: object}>();

  public openFolder = function(obj){
    this.onCurrentFolderChange.emit({
      name: obj.name,
      backtrack: obj.backtrack,
      children: obj.children
    });

    if (obj.hasOwnProperty('children') && obj.children.length > 0) {
      this.hasChildren = true;
      this.errorMsg = false;
      // this.curFolder = obj;
    } else {
      this.hasChildren = false;
      this.errorMsg = true;
    }
  };

  public checkDuplicates = function (folder, name) {
    if (!folder.hasOwnProperty('children') || folder.children === undefined || folder.children.length === 0 || folder.children === [{}]) {
      return false;
    }else {
      const childs = folder.children;
      childs.forEach(function(val) {
        if (val.name === name) {
          return true;
        }
      });
    }
    return false;
  };



  public onCreate = function (folder) {

    if (folder.name === '.' && folder.children.length === 0) {
      this.newFolder = {
        name: '.',
        id: 0,
        backtrack: ['[root]'],
        children: [
          {
            name: this.newFolderName,
            id: 1,
            backtrack: ['[root]', this.newFolderName],
            children: [

            ]

          }
        ]

      };
      localStorage.setItem('folder', JSON.stringify(this.newFolder));
      localStorage.setItem('currentId', JSON.stringify(2));
      this.curFolder = this.newFolder;
      this.showPopup = false;
      this.errorMsg = false;

    }else {

      if (!this.checkDuplicates (folder, this.newFolderName)) {
        let tempObj = {};
        const newBt = JSON.parse(localStorage.getItem('currentPath'));
        const oldFolder = JSON.parse(localStorage.getItem('folder'));
        this.updatedBt = [];
        this.updatedBt = newBt;
        this.updatedBt.push(this.newFolderName);
        tempObj = {
          name: this.newFolderName,
          id: this.incrId(),
          backtrack: this.updatedBt,
          children: []
        };

        const currtFolder = oldFolder;
        const childrens = currtFolder.children;
        let refFolder = {};
        console.log(newBt);
        if (newBt.length === 2) {
          currtFolder.children.push(tempObj);
          console.log(currtFolder);
          this.curFolder = oldFolder;
          localStorage.setItem('folder', JSON.stringify(currtFolder));
          this.showPopup = false;
          this.errorMsg = false;
        }else {

          newBt.forEach(function(val, i){
            console.log(i);
            currtFolder.children.forEach( function (val1){
              if (val !== '[root]' && i !== newBt.length - 1) {
                console.log(val1.name);
                if (val1.name === val) {
                  if (i === newBt.length - 2) {

                    val1.children.push(tempObj);
                    refFolder = val1;

                    localStorage.setItem('folder', JSON.stringify(currtFolder));
                  }
                }
              }

            });
          });

          localStorage.setItem('folder', JSON.stringify(currtFolder));
          this.showPopup = false;
          this.errorMsg = false;
          this.curFolder = refFolder;
        }
      }
    }


  };

  public addFolder(folder) {
    this.showPopup = true;
  }

  public incrId() {
      const id = JSON.parse(localStorage.getItem('currentId'));
      const idVal = parseInt(id, 0) + 1;
      localStorage.setItem('currentId', JSON.parse(idVal.toString()));
      return parseInt(id, 0) + 1;
  }

  constructor() {

  }

  ngOnInit() {
    if (this.curFolder.children === [] || this.curFolder === undefined) {
      this.hasChildren = false;
      this.errorMsg = true;
    }
  }


}
