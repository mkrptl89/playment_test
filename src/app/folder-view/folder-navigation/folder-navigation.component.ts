import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-folder-navigation',
  templateUrl: './folder-navigation.component.html',
  styleUrls: ['./folder-navigation.component.css']
})
export class FolderNavigationComponent implements OnInit {

  @Input() curPath: string;

  constructor() { }

  ngOnInit() {
  }

}
